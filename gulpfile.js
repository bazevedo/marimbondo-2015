'use strict';

/* GULP PLUGINS
========================================================================== */
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	prefix = require('gulp-autoprefixer'),
	autoprefixer = require('autoprefixer-core'),
	rename = require('gulp-rename'),
 	livereload = require('gulp-livereload'),
	watch = require('gulp-watch'),
	shell = require('gulp-shell'),
	minifyCSS = require('gulp-minify-css'),
	imagemin = require('gulp-imagemin'),
	pngcrush = require('imagemin-pngcrush'),
	postcss = require('gulp-postcss'),
	changed = require('gulp-changed'),
	webp = require('gulp-webp'),
	sourcemaps = require('gulp-sourcemaps');

/* CONFS
========================================================================== */
var processors = [
    autoprefixer('last 15 version')
];

livereload({ start: true });

/* DEV TASKS
========================================================================== */

//Compile the Sass css, prefix the styles and create the sourceMap for the dev CSS
gulp.task('css', function() {

	return gulp.src('dev/sass/main-sass.scss')
	.pipe(sourcemaps.init())
	.pipe(sass({ errLogToConsole:true }))
	.pipe(postcss(processors))
	.pipe(rename('main.css'))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('dev/css'));

});

//Creates the webImages from the jpg/png ones.
gulp.task('webpImages', function () {
    return gulp.src('assets/img/**/*')
        .pipe(webp())
        .pipe(gulp.dest('assets/img'));
});

/* PROD TASKS
========================================================================== */

//Minify the dev CSS for production
gulp.task('minifyCSS', function () {

	return gulp.src('dev/sass/main-sass.scss')
	.pipe(sass({ errLogToConsole:true }))
	.pipe(prefix('last 15 version'))
	.pipe(minifyCSS({keepSpecialComments:0}))
	.pipe(rename('main.css'))
	.pipe(gulp.dest('assets/css'));

});

//Minify the images
gulp.task('imageMin', function () {

    return gulp.src('dev/img/**/*')
    	.pipe(changed('assets/img'))
        .pipe(imagemin({
            optimizationLevel : 7
           // use: [pngcrush()]
        }))
        .pipe(gulp.dest('assets/img'));

});

//Call the requireJS grunt plugin
gulp.task('requireJS', shell.task([
	'grunt requirejs'
]));


/* DEV/BUILD tasks
========================================================================== */

gulp.task('dev', function() {

	//Creates the liveReload Server
	var server = livereload();

	gulp.watch('dev/sass/**/*.scss', ['css']);

	gulp.watch(['dev/css/main.css', '**/*.php', 'dev/js/**/*.js']).on('change', function(file) {
		server.changed(file.path);
	});

});

gulp.task('build', function() {

	gulp.start('minifyCSS');
	gulp.start('imageMin');
	gulp.start('requireJS');

});