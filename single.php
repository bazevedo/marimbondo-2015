<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

<?php get_template_part('sections/inside-header'); ?>
<main class="section module module--page">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article <?php post_class('post-container content') ?> id="post-<?php the_ID(); ?>">

			<h1 class="default-text--page-title default-text default-text--size-11 default-text--color-1 default-text--light default-text--title"><?php the_title(); ?></h1>
			<time class="post-container__time default-text default-text--size-3 default-text--color-3 default-text--title" datetime="<?php echo date(DATE_W3C); ?>" ><?php the_time(get_option('date_format')) ?></time>

			<div class="entry-content default-text default-text--size-4 default-text--color-5">

				<?php the_content(); ?>

			</div>
			<footer class="all-posts-container">
				<a class="anchor-button anchor-button--type-2" href="<?php bloginfo('url'); ?>/blog">Voltar para a lista de notícias</a>
			</footer>
		</article>

	<?php endwhile; endif; ?>
</main>
<section class="section more-news-section module module-page">
	<div class="content more-news-wrapper">
		<h2 class="default-text default-text--title default-text--light default-text--size-8 default-text--color-1 default-text--section-title">Veja Mais notícias</h2>
		<ul class="more-news-list">
			<?php

				//Gets the first 3 posts from the blog and exclude the current one
				$postid = array(get_the_ID());
		       	$query = new WP_Query(array(
				    'posts_per_page'   => 3,
				    'orderby' => 'rand',
				    'post__not_in' => $postid
		        ));

			?>
			<?php while ($query->have_posts()): $query->the_post(); ?>
					<?php $imageLink = wp_get_attachment_image_src(get_post_thumbnail_id(), 'featured-prime'); ?>

					<li class="more-news-list__item col-4-12" id="post-<?php the_ID(); ?>">
						<a class="more-news-list__anchor" href="<?php the_permalink() ?>">
							<img class="more-news-list__img" alt="<?php echo get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true); ?>" src="<?php echo $imageLink[0]; ?>" />
							<h3 class="more-news-list__title default-text default-text--bold default-text--size-4"><?php the_title(); ?></h3>
						</a>
					</li>

		 	<?php endwhile; ?>

		</ul>
	</div>
</section>
<?php get_template_part('sections/footer'); ?>

<?php get_footer(); ?>