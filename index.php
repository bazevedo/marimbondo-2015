<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>


<?php get_template_part('sections/home-header'); ?>
<?php get_template_part('sections/about-us'); ?>
<?php get_template_part('sections/works'); ?>
<hr class="content horizontal-breaker horizontal-breaker--testimonials" />
<?php get_template_part('sections/testimonials'); ?>
<?php get_template_part('sections/contact'); ?>
<?php get_template_part('sections/footer'); ?>

<?php get_footer(); ?>