<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */

	// Options Framework (https://github.com/devinsays/options-framework-plugin)
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/_/inc/' );
		require_once dirname( __FILE__ ) . '/_/inc/options-framework.php';
	}

	// Theme Setup (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_setup() {
		load_theme_textdomain( 'html5reset', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
		add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status' ) );
		register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );
		add_theme_support( 'post-thumbnails' );
	}
	add_action( 'after_setup_theme', 'html5reset_setup' );

	// Scripts & Styles (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_scripts_styles() {
		global $wp_styles;
	}
	add_action( 'wp_enqueue_scripts', 'html5reset_scripts_styles' );

	// WP Title (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() )
			return $title;

//		 Add the site name.
		$title .= get_bloginfo( 'name' );

//		 Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page %s', 'html5reset' ), max( $paged, $page ) );

		return $title;
	}
	add_filter( 'wp_title', 'html5reset_wp_title', 10, 2 );




//OLD STUFF BELOW


	// Load jQuery
	if ( !function_exists( 'core_mods' ) ) {
		function core_mods() {
			if ( !is_admin() ) {
				wp_deregister_script( 'jquery' );
				wp_register_script( 'jquery', ( "//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" ),  false, null, true);
				wp_enqueue_script( 'jquery' );
			}
		}
		add_action( 'wp_enqueue_scripts', 'core_mods' );
	}

	// Clean up the <head>, if you so desire.
	//	function removeHeadLinks() {
	//    	remove_action('wp_head', 'rsd_link');
	//    	remove_action('wp_head', 'wlwmanifest_link');
	//    }
	//    add_action('init', 'removeHeadLinks');

	// Custom Menu
	register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );

	// Widgets
	if ( function_exists('register_sidebar' )) {
		function html5reset_widgets_init() {
			register_sidebar( array(
				'name'          => __( 'Sidebar Widgets', 'html5reset' ),
				'id'            => 'sidebar-primary',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
		}
		add_action( 'widgets_init', 'html5reset_widgets_init' );
	}

	// Navigation - update coming from twentythirteen
	function post_navigation() {
		echo '<div class="navigation">';
		echo '	<div class="next-posts">'.get_next_posts_link('&laquo; Older Entries').'</div>';
		echo '	<div class="prev-posts">'.get_previous_posts_link('Newer Entries &raquo;').'</div>';
		echo '</div>';
	}

	// Posted On
	function posted_on() {
		printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_author() )
		);
	}

	//Get the current environment. We need this to ensure that our dev assets don't go to production
	function getEnvironment() {
		$templatePath;

	    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "127.0.0.1") {
	       $templatePath = 'dev';
	    } else {
	        $templatePath = 'assets';
	    }

	    return $templatePath;

	}

	//Remove the comments injected styles
	function removeRecentCommentsStyle() {
		add_filter( 'show_recent_comments_widget_style', '__return_false' );
	}
	add_action( 'widgets_init', 'removeRecentCommentsStyle' );


	//Custom Thumb sizes
	if ( function_exists( 'add_theme_support' ) ) {

		//For the featured thumbs in the home list
		add_image_size('featured-prime', 500, 9999);

		//For the athletes testimonials @home
		add_image_size('testimonial-thumb', 80, 80);

		//For the news thumbs @home, besides the first one
		add_image_size('featured-small', 260, 333, true);

	}

	//Get´s the Youtube Video URL and Iframe
	function returnYoutubeID ($iframeHTML) {
		//This Regex will look for the embed direct id
		preg_match('/src="([^"]+)"/', $iframeHTML, $match);

		// If the match is an array, then we already have the embed id. If it´s not, we will stripe the id from the default youtube url
		if ($match) {

			$url = $match[1];
			$url = htmlspecialchars($url);
			echo '<iframe src='. $url .' allowfullscreen></iframe>';

		} else {

			//We use this to get the "v" variable from the youtube url
			parse_str( parse_url( $iframeHTML, PHP_URL_QUERY ), $embedVariables );

			echo $embedVariables['v'];

		}

	}


	//Remove rss feeds and other non significant links
	remove_action ('wp_head', 'rsd_link');
	remove_action( 'wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3); // Remove category feeds
	remove_action('wp_head', 'feed_links', 2); // Remove Post and Comment Feeds
	function remove_feed () {
	   remove_theme_support( 'automatic-feed-links' );
	}
	add_action( 'after_theme_support', 'remove_feed' );


	function returnYoutubeThumb ($iframeHTML) {

		//We use this to get the "v" variable from the youtube url
		parse_str( parse_url( $iframeHTML, PHP_URL_QUERY ), $embedVariables );

		$thumbURL = 'http://img.youtube.com/vi/'.$embedVariables['v'].'/0.jpg';
		$iframe = '<iframe src=https://www.youtube.com/embed/'. $embedVariables['v'] .' allowfullscreen></iframe>';

		$embedVariables['thumb'] = $thumbURL;
		$embedVariables['id'] = $embedVariables['v'];
		$embedVariables['iframe'] = $iframe;

		return $embedVariables;

	}

?>