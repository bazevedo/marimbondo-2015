'use strict';
module.exports = function(grunt) {

	//Load all the grunt tasks
	require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

	rsync : {
		options : {
			exclude : ['.git*','node_modules'],
			recursive : true
		},
		home : {
			options: {
                src: '.',
                dest: 'www/beta/wp-content/themes/marimbondo',
                host: 'marimbondo@marimbondo.me',
                recursive: true,
                syncDest: true,
                exclude: [".git*","node_modules", ".DS_Store*"]
        	}
		},
        prod : {
            options: {
                src: '.',
                dest: 'www/wp-content/themes/marimbondo',
                host: 'marimbondo@marimbondo.me',
                recursive: true,
                syncDest: true,
                exclude: [".git*","node_modules", ".DS_Store*"]
            }
        }
	},

    requirejs: {
        app: {
            options: {
                findNestedDependencies: true,
                mainConfigFile: 'dev/js/main.js',
                baseUrl : 'dev/js/',
                out : 'assets/js/main-min.js',
                name : 'main',
                include: ['require.js', 'plugins/sequence.js'],
                optimize : 'uglify2'
            }
        }
    }

  });

  grunt.registerTask('default', ['parallel']);

}