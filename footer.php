


	</div>

	<?php wp_footer(); ?>

<?php  if (getEnvironment() === 'dev') { ?>

  <script src="<?php echo get_template_directory_uri(); ?>/dev/js/plugins/sequence.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/js/require.js" data-main="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/js/main"></script>

<?php } else { ?>

  <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/assets/js/lib/jquery-1.11.2.min.js"><\/script>')</script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main-min.js"></script>

<?php } ?>


<?php
  if (getEnvironment() === 'assets') { ?>

  <script>

    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-36410906-1','auto');ga('send','pageview');

  </script>
<?php } ?>
</body>

</html>
