(function () {

	'use strict';

	var parallax = {},
		lastPosition = window.pageYOffset,
		cssTransform,
        isFirefox = /firefox/.test(navigator.userAgent.toLowerCase()),
		containers = {
			uptown : document.getElementById('js-home-header__uptown'),
			middletown : document.getElementById('js-home-header__middletown'),
			lowertown : document.getElementById('js-home-header__lowertown')
		};

	parallax = {

		init : function () {

			//First we configure all the animations variables: RaF and vendor prefixes
			parallax.configureAnimations();

			//We do this once, just to kick the animations
			parallax.applyParallax();

			parallax.bindEvents();

		},

		configureAnimations : function () {

			parallax.configureRAF();

			parallax.configureVendorPrefix();

		},

		configureRAF : function () {

			if ( !window.requestAnimationFrame ) {

				window.requestAnimationFrame = ( function() {

					return window.webkitRequestAnimationFrame ||
					window.mozRequestAnimationFrame ||
					window.oRequestAnimationFrame ||
					window.msRequestAnimationFrame ||
					function( /* function FrameRequestCallback */ callback, /* DOMElement Element */ element ) {

						window.setTimeout( callback, 1000 / 60 );

					};

				} )();

			};

		},

		configureVendorPrefix : function () {

			cssTransform = (function () {

			    var prefixes = 'transform webkitTransform mozTransform oTransform msTransform'.split(' '),
			    	el = document.createElement('div'),
			      	cssTransform,
			      	i = 0;

			    while( cssTransform === undefined ) {

			        cssTransform = el.style[prefixes[i]] !== undefined ? prefixes[i] : undefined;
			        i++;

			     }

			     return cssTransform;

			 })();

		},

		bindEvents : function () {

			window.addEventListener('scroll', parallax.verifyRAF, false);

		},

		verifyRAF : function () {

			if (lastPosition !== window.pageYOffset) {

				lastPosition = window.pageYOffset;

				window.requestAnimationFrame(parallax.applyParallax);

			}

		},

		applyParallax : function () {

			if (!isFirefox) {

				containers.uptown.style[cssTransform] = 'translate3d(0, ' + lastPosition * 0.3 + 'px, 0)';
				containers.middletown.style[cssTransform] = 'translate3d(0, -' + lastPosition * 0.1 + 'px, 0)';
				containers.lowertown.style[cssTransform] = 'translate3d(0, -' + lastPosition * 0.4 + 'px, 0)';

			} else {

				containers.lowertown.style[cssTransform] = 'translate3d(0, -' + lastPosition * 0.4 + 'px, 0)';

			}

		}

	};

	return parallax.init();

})();