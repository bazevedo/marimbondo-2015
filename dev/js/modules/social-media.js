/*global $*/
(function () {

	'use strict';

	var socialMedia = {},
		facebookLikeContainer = $('#js-facebook-frame');

	socialMedia = {

		init : function () {

			socialMedia.loadFacebookLike();

		},

		loadFacebookLike : function () {

			var facebookIframeURL = '<iframe title="Facebook LikeBox" class="social-networks-container__box" src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fmarimbondo.me&amp;width&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" allowTransparency="true"></iframe>';

			facebookLikeContainer.html(facebookIframeURL);

		}

	};

	return socialMedia.init();

})();