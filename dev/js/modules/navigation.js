/*global $*/
(function () {

	'use strict';

	var navigation = {},
		navigationWrapper = $('#js-navigation-wrapper'),
		navigationList = $('#js-navigation-list'),
		mobileNavigationToggler = $('#js-navigation-toogler'),
		hamburguerButton = mobileNavigationToggler.find('.navigation-toogler__burguer:first'),
		acessibilityMenuContainer = $('#js-acessibility-menu'),
		screenSize = document.documentElement.clientWidth || window.innerWidth,
		documentHooks = $('html, body'),
		isSmallScreen = screenSize <= 1024 ? true : false;

	navigation.options = {};
	navigation.options.oppened = false;

	navigation.methods = {

		init : function () {

			navigation.methods.bindMobileBehaviours();

			if (acessibilityMenuContainer.length) {

                acessibilityMenuContainer.on('click', 'a', function (evt) {

                	var selectedSubmenu = $(this);

					navigation.methods.loadAccessibleBehaviours(selectedSubmenu.attr('href'));

                });
			}

			navigationList.on('click', 'a', function (evt) {

				var _this = $(this);

				if (!_this.data('external')) {

                    evt.preventDefault();

                }

                navigation.selectedAnchorId = _this.attr('href');

                navigation.methods.goToSelectedAnchor(navigation.selectedAnchorId);

			});

		},

		goToSelectedAnchor : function (anchorID) {

			if (navigation.options.oppened) {

				navigation.methods.toogleNavigation();

			}

            navigation.anchorContainer = $(anchorID);

            documentHooks.animate({

                scrollTop: navigation.anchorContainer.offset().top - 80

            }, 700, function () {

                navigation.anchorContainer.attr('tabindex', 0);
            	navigation.anchorContainer.focus();

            });

		},

		bindMobileBehaviours : function () {

			mobileNavigationToggler.on('click', function () {

				navigation.methods.toogleNavigation();

			});

	        //Closes the menu window with the keyboard 'escape' key
	        $(document).keyup(function (evt) {

	            if (evt.keyCode === 27 && navigation.options.oppened) {

	                navigation.methods.toogleNavigation();

	            }

	        });

		},

		toogleNavigation : function () {

			if (navigation.options.oppened) {

				$(document.body).off('click.openMenu');

				navigation.options.oppened = false;

			} else {

				navigation.options.oppened = true;

				navigation.methods.outsideClickCloseMenu();

			}

			navigationWrapper.toggleClass('navigation-wrapper--open');
			hamburguerButton.toggleClass('navigation-toogler__burguer--close');
			navigationList.toggleClass('navigation-list--open');

		},

        outsideClickCloseMenu : function () {

            //We need the setTimeout
            setTimeout(function () {

                $(document.body).on('click.openMenu', function (evt) {

                    if ($(evt.target).is(navigationWrapper) && navigation.options.oppened) {

                        navigation.methods.toogleNavigation();

                    }

                });

            }, 0);

        },

		loadAccessibleBehaviours : function (anchorID) {

			navigation.anchorContainer = $(anchorID);

	        navigation.anchorContainer.attr('tabindex', 0);
	    	navigation.anchorContainer.focus();

		}

	};

	return navigation.methods.init();

})();