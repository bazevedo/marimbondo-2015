/*global alert, $*/
(function () {

	'use strict';

	var contactForm = {},
		contactFormContainer = $('#js-contact-form'),
		formFields = {
			name : $('#contact-name'),
			email : $('#contact-mail')
		},
		validateMailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	contactForm = {

		init : function () {

			contactForm.bindEvents();

		},

		bindEvents : function () {

			contactFormContainer.on('submit', function (evt) {

				evt.preventDefault();

				contactForm.verifyFields($(this));

			});

		},

		verifyFields : function (form) {

			if ($.trim(formFields.name.val()).length === 0) {

				alert('Por favor, preencha seu nome.');
				formFields.name.focus();

			} else if ($.trim(formFields.email.val()).length === 0 || !validateMailRegex.test(formFields.email.val())) {

				alert('Por favor, preencha corretamente seu e-mail.');
				formFields.email.focus();

			} else {

				contactForm.submitForm(form);

			}

		},

		submitForm : function (form) {

			$.ajax({
				url : window.mrbAssets + '/scripts/sendMail.php?' + form.serialize(),
				success : function (res) {

					if (res === 'success') {

						contactForm.cleanFormFields(form);

						alert('Obrigado pelo contato :)');

					} else {

						alert('Por favor, verifique novamente os campos preenchidos');

					}

				}
			});

		},

		cleanFormFields : function (form) {

			form.trigger('reset');

		}

	};

	return contactForm.init();

})();