/*global $*/
(function () {

	'use strict';

	var principles = {},
        containers = {},
        configs = {};

    configs = {
        layerIsOpen : false , //To see if the principles are already active
        lastOpened : false,
        currentPrinciple : false,
        windowHash : window.location.hash
    };

    containers = {
        sequenceContainer : $('#js-sequence'),
        principlesList : $('#js-principles-list'),
        overlayContainer : $('#js-principles-overlay'),
        closeButton : $('#js-principles-commmands__close'),
        principlesCommands : $('#js-principles-commmands')
    };

    containers.overlayContainerList = containers.overlayContainer.find('li');
    containers.principlesListAnchors =  containers.principlesList.find('a');
    containers.principlesCommandsButtons =  containers.principlesCommands.find('button');

    window.screenSize = document.documentElement.clientWidth || window.innerWidth;
    window.isMobileScreen = window.screenSize < 768 ? true : false;

    principles = {

        loadSequence : function () {

                $(document).ready(function() {
                    var options = {
                        autoPlay : false,
                        nextButton : $('#js-principles-commmands__next'),
                        prevButton : $('#js-principles-commmands__prev'),
                        transitionThreshold : 200,
                        preloader : true,
                        reverseAnimationsWhenNavigatingBackwards : false,
                        preventDelayWhenReversingAnimations : true,
                        keyNavigation : false

                    };

                    window.sequence = containers.sequenceContainer.sequence(options).data('sequence');

                    principles.init();

                });

        },

        init : function () {

            //We will put a callback before the next element is loaded
            principles.loadSequenceCallbacks();

            //Open the speaker if a hash is passed in the URL on page load
            if (configs.windowHash) {

                principles.initByHash();

            }

            containers.principlesList.on('click', 'a', function (evt) {

                evt.preventDefault();

                var _this = $(this),
                    principleAnchor = _this.attr('href');

                //Remember the last opened item. When we close the layer, this item will be focused.
                configs.lastOpened = _this;

                principles.prepareLayer($(principleAnchor));

            });

            containers.closeButton.on('click', function () {

                if (configs.layerIsOpen) {

                    principles.closeLayer();

                }

            });

            $(document).keyup(function (evt) {

                if (evt.keyCode === 27 && configs.layerIsOpen) {

                    principles.closeLayer();

                } else if ((evt.keyCode === 37 || evt.keyCode === 39) && configs.layerIsOpen) {

                    if (evt.keyCode === 37) {

                        principles.keyboardNavigation('prev');

                    } else {

                        principles.keyboardNavigation('next');

                    }

                }

            });

        },

        keyboardNavigation : function (direction) {

            if (direction === 'next') {

                window.sequence.next();

            } else if (direction === 'prev') {

                window.sequence.prev();

            }

        },

        initByHash : function () {

            var hashedLayerContainer = $(configs.windowHash);

            if (hashedLayerContainer.length) {

                if (hashedLayerContainer.parents('li').data('principle') === hashedLayerContainer.attr('id')) {

                    //We need to initiate sequence. This way we don´t have any flickering in the screen
                    window.sequence.next();

                    //After the initial movement, we do the normal navigation.
                    setTimeout(function () {

                        principles.prepareLayer(hashedLayerContainer);

                    }, 0);

                }

            }

        },

        prepareLayer : function (selectedPrinciple) {

                var selectedPrincipleParentList = selectedPrinciple.parents('li');

                principles.principlesIndex = selectedPrincipleParentList.index();

                //We need to store the current principle to remove the class
                configs.currentPrinciple = selectedPrincipleParentList.attr('data-principle');

                containers.principlesList.addClass('principles-list--is-open principles-list--current-' + configs.currentPrinciple +'');

                principles.openLayer(principles.principlesIndex);

        },

        openLayer : function (principleID) {

            var nextFrameId = window.sequence.nextFrameID,
                nextFrameNode = $(containers.overlayContainerList[nextFrameId - 1]);

            configs.layerIsOpen = true;

            //Removes the class that hides the layer
            setTimeout(function () {
                containers.sequenceContainer.toggleClass('visuallyhidden');
            }, 400);

            //Hide the focus from the original list of principles
            containers.principlesListAnchors.each(function () {
                $(this).attr('tabindex', -1);
            });

            //Enables focus for the layer commands
            containers.principlesCommandsButtons.each(function () {
                $(this).attr('tabindex', 0);
            });

            window.sequence.goTo(principleID + 1, 1);

            //Focus the title of the selected principle
            principles.focusSelectedPrinciple(nextFrameNode);

        },

        loadSequenceCallbacks : function () {

            //Before the next speaker animation, we will change the container class to be the same as the speaker room
            window.sequence.beforeNextFrameAnimatesIn = function () {

                var nextFrameId = window.sequence.nextFrameID,
                    nextFrameNode = $(containers.overlayContainerList[nextFrameId - 1]);

                containers.principlesList.removeClass('principles-list--current-' + configs.currentPrinciple + '');

                configs.currentPrinciple = nextFrameNode.data('principle');

                containers.principlesList.addClass('principles-list--current-' + configs.currentPrinciple + '');

                //For acessibility porposes we will focus the name of the principle
                principles.focusSelectedPrinciple(nextFrameNode);

            };

        },

        focusSelectedPrinciple : function (nextSpeakerNode) {

            var currentNodeTitle = nextSpeakerNode.find('.principles-overlay__title:first');

            if (!configs.currentNodeTitle) {

                configs.currentNodeTitle = currentNodeTitle;

            }

            if (configs.currentNodeTitle !== currentNodeTitle) {

                configs.currentNodeTitle.removeAttr('tabindex');

                currentNodeTitle.attr('tabindex', 0);
                currentNodeTitle.focus();

                configs.currentNodeTitle = currentNodeTitle;

            }

        },

        closeLayer : function () {

            configs.layerIsOpen = false;

            containers.sequenceContainer.toggleClass('visuallyhidden');

            containers.principlesList.removeClass('principles-list--is-open principles-list--current-' + configs.currentPrinciple + '');

            configs.lastOpened.focus();

            containers.principlesListAnchors.each(function () {
                $(this).attr('tabindex', 0);
            });

            containers.principlesCommandsButtons.each(function () {
                $(this).attr('tabindex', -1);
            });

            configs.currentNodeTitle.removeAttr('tabindex');

        }

    };

    //We need to load sequence before anything.
    principles.loadSequence();

}());