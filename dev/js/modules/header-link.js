/*global $*/
(function () {

	'use strict';

	var headerLink = {},
		contactLinkContainer = $('#js-home-header__lets-work');

	headerLink = {

		init : function () {

			if (contactLinkContainer.length) {

				headerLink.bindEvents();

			}

		},

		bindEvents : function () {

			contactLinkContainer.on('click', function (evt) {

				evt.preventDefault();

				var _this = $(this),
					anchorContainer = $(_this.attr('href'));

				headerLink.animateAnchor(anchorContainer);

			});

		},

		animateAnchor : function (anchorContainer) {

			$('html, body').animate({

                scrollTop: anchorContainer.offset().top - 80

            }, 700, function () {

                anchorContainer.attr('tabindex', 0);
            	anchorContainer.focus();

            });

		}

	};

	return headerLink.init();

})();