/*global $, require, FastClick*/
(function () {

	'use strict';

    var PUBLIC = {},
        PRIVATE = {};

    PRIVATE.init = function () {

        //configure our requireJS paths
        window.configRequirePaths = function () {

            require.config({
                paths: {
                    'fastClick' : 'plugins/fastclick',
                    'navigation' : 'modules/navigation',
                    'headerLink' : 'modules/header-link',
                    'contactForm' : 'modules/contact-form',
                    'parallax' : 'modules/parallax',
                    'principles' : 'modules/principles',
                    'socialMedia' : 'modules/social-media'
                }
            });

        };

        window.screenSize = document.documentElement.clientWidth || window.innerWidth;
        window.isSmallScreen = window.screenSize <= 1024 ? true : false;
        window.isMobileScreen = window.screenSize <= 768 ? true : false;

        //We need to do this because firefox can´t apply "transform-origin" in a SVG. The bug is open @ https://bugzilla.mozilla.org/show_bug.cgi?id=923193
        if (/firefox/.test(navigator.userAgent.toLowerCase())) {
            document.body.className += ' firefox';
        }

        PUBLIC.init();

    };

	PUBLIC.init = function () {


        window.configRequirePaths();

        //If our user is visiting us in a small screen, let's apply fastClick
        if (window.isSmallScreen) {

            PUBLIC.applyFastClick();

        }

		PUBLIC.navigation();

        PUBLIC.headerLink();

        PUBLIC.contactForm();

        PUBLIC.homeParallax();

        PUBLIC.principles();

        PUBLIC.socialMedia();

	};

    PUBLIC.applyFastClick = function () {

        require(['fastClick'], function () {

            FastClick.attach(document.body);

        });

    };

    PUBLIC.navigation = function () {

        var navigationContainer = $('#js-navigation-wrapper');

        if (navigationContainer) {

            require(['navigation']);

        }

    };

    PUBLIC.headerLink = function () {

        var contactLinkContainer = document.getElementById('js-home-header__lets-work');

        if (contactLinkContainer) {

            require(['headerLink']);

        }

    };

    PUBLIC.contactForm = function () {

        var contactFormContainer = document.getElementById('js-contact-form');

        if (contactFormContainer) {

            require(['contactForm']);

        }
    };

    PUBLIC.homeParallax = function () {

        require(['parallax']);

    };


    PUBLIC.principles = function () {

        var principlesContainer = document.getElementById('js-principles-list');

        if (principlesContainer) {

            require(['principles']);

        }

    };

    PUBLIC.socialMedia = function () {

        var facebookLikeContainer = document.getElementById('js-facebook-frame');

        if (facebookLikeContainer) {

            require(['socialMedia']);

        }

    };

    return PRIVATE.init();

})();