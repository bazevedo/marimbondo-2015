<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header();

 ?>
 <?php get_template_part('sections/inside-header'); ?>
<section class="section module module--page">
    <div class="content">
		<h1 class="col-8-12 default-text--page-title default-text default-text--size-11 default-text--color-1 default-text--light default-text--title">404</h1>
		<p class="col-12-12">Não encontramos a página que você acessou. Por favor, volte para <a class="default-text--color-4" href="<?php bloginfo('url'); ?>" title="Voltar para Home">Home</a> ou acesse um dos itens do menu.</p>
    </div>
</section>
<?php get_template_part('sections/footer'); ?>
<?php get_footer(); ?>