
<svg class="principles-icon principles-icon--responsive" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="198.75px" height="133px" viewBox="0 0 198.75 133" enable-background="new 0 0 198.75 133" xml:space="preserve">
<g class="computer">
	<path fill="#33485D" d="M178.1,106.1c0,2.711-2.209,4.91-4.939,4.91H24.51c-2.72,0-4.93-2.199-4.93-4.91V11.41
		c0-2.71,2.21-4.91,4.93-4.91h148.65c2.73,0,4.939,2.2,4.939,4.91V106.1z"/>
	<path fill="#9D9DA4" d="M1.515,113.41c2.3,2.99,7.94,6.09,11.33,6.09h172.434c3.391,0,7.931-3.1,9.75-6.09H1.515z"/>
	<path fill="#C8C7C4" d="M195.5,113.359c0,0.49-0.4,0.891-0.9,0.891H2.4c-0.5,0-0.9-0.4-0.9-0.891v-1.569c0-0.49,0.4-0.89,0.9-0.89
		h192.2c0.5,0,0.9,0.399,0.9,0.89V113.359z"/>
	<rect x="27.811" y="14.69" fill="#FFFFFF" width="141.909" height="88.43"/>
	<path fill="#8C8C94" d="M80.59,111.01v0.08c0,0.74,0.61,1.34,1.35,1.34h33.601c0.75,0,1.351-0.6,1.351-1.34v-0.08H80.59z"/>
	<path fill="#5A6A7A" d="M100.5,10.79c0,0.95-0.77,1.72-1.721,1.72c-0.949,0-1.719-0.77-1.719-1.72c0-0.94,0.77-1.71,1.719-1.71
		C99.73,9.08,100.5,9.85,100.5,10.79z"/>
	<rect x="32.779" y="19.67" fill="#FFD060" width="133.41" height="23.51"/>
	<rect x="32.939" y="81.98" fill="#426757" width="65.051" height="15.549"/>
	<rect x="45.82" y="26.74" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="103.3" height="1.81"/>
	<g>
		<rect x="45.82" y="33.69" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="44.55" height="1.81"/>
		<rect x="96.5" y="33.69" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="52.3" height="1.81"/>
	</g>
	<rect x="101.13" y="47.15" fill="#F36B54" width="65.06" height="50.379"/>
	<rect x="115.72" y="60.88" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="35.88" height="1.81"/>
	<g>
		<rect x="115.72" y="67.61" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="14.171" height="1.81"/>
		<rect x="133.43" y="67.61" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="18.17" height="1.81"/>
	</g>
	<g>
		<rect x="145.09" y="80.92" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.51" height="1.811"/>
		<rect x="115.72" y="80.92" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="26.04" height="1.811"/>
	</g>
	<g>
		<rect x="115.72" y="74.21" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="3.841" height="1.81"/>
		<rect x="122.96" y="74.21" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="15.229" height="1.81"/>
		<rect x="141.1" y="74.21" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="10.5" height="1.81"/>
	</g>
	<rect x="32.939" y="47.15" fill="#4FA8C9" width="65.051" height="31.61"/>
	<rect x="43.93" y="86.57" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="41.23" height="1.82"/>
	<g>
		<rect x="43.93" y="91.939" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="17.69" height="1.811"/>
		<rect x="64.061" y="91.939" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="20.77" height="1.811"/>
	</g>
	<g>
		<rect x="43.76" y="55.29" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="16.93" height="1.82"/>
		<rect x="64.92" y="55.29" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="21.7" height="1.82"/>
	</g>
	<g>
		<rect x="78.85" y="68.609" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="7.771" height="1.811"/>
		<rect x="43.76" y="68.609" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="31.11" height="1.811"/>
	</g>
	<g>
		<rect x="43.76" y="61.89" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="4.59" height="1.82"/>
		<rect x="52.41" y="61.89" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="18.189" height="1.82"/>
		<rect x="74.09" y="61.89" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="12.53" height="1.82"/>
	</g>
</g>
<g class="tablet">
	<path fill="#33485D" d="M189.324,124.98c0,1.47-1.199,2.66-2.68,2.66h-53.92c-1.48,0-2.68-1.19-2.68-2.66V44.78
		c0-1.47,1.199-2.66,2.68-2.66h53.92c1.48,0,2.68,1.19,2.68,2.66V124.98z"/>
	<path fill="#5A6A7A" d="M160.795,46.36c0,0.65-0.531,1.17-1.181,1.17s-1.18-0.52-1.18-1.17s0.53-1.17,1.18-1.17
		S160.795,45.71,160.795,46.36z"/>
	<rect x="133.314" y="50.63" fill="#FFFFFF" width="52.85" height="66.899"/>
	<path fill="#293A4C" d="M164.424,122.46c0,0.86-0.709,1.56-1.58,1.56h-6.189c-0.87,0-1.57-0.699-1.57-1.56s0.7-1.56,1.57-1.56
		h6.189C163.715,120.9,164.424,121.6,164.424,122.46z"/>
	<g>
		<rect x="135.344" y="54.46" fill="#FFD060" width="48.801" height="15.38"/>
		<rect x="140.854" y="59.07" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="37.78" height="1.22"/>
		<g>
			<rect x="140.914" y="63.62" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="16.29" height="1.22"/>
			<rect x="159.444" y="63.62" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="19.13" height="1.22"/>
		</g>
	</g>
	<g>
		<rect x="161.744" y="72.439" fill="#F36B54" width="22.4" height="42.94"/>
		<g>
			<rect x="166.385" y="80.46" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="13.119" height="1.22"/>
			<g>
				<rect x="166.385" y="84.74" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="5.18" height="1.22"/>
				<rect x="172.854" y="84.74" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.649" height="1.22"/>
			</g>
			<g>
				<rect x="177.125" y="93.3" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="2.379" height="1.22"/>
				<rect x="166.385" y="93.3" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="9.52" height="1.22"/>
			</g>
			<g>
				<rect x="166.385" y="89.02" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="1.399" height="1.221"/>
				<rect x="169.024" y="89.02" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="5.569" height="1.221"/>
				<rect x="175.664" y="89.02" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="3.84" height="1.221"/>
			</g>
			<g>
				<rect x="166.385" y="97.58" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="5.18" height="1.22"/>
				<rect x="172.854" y="97.58" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.649" height="1.22"/>
			</g>
			<g>
				<rect x="177.125" y="106.141" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="2.379" height="1.219"/>
				<rect x="166.385" y="106.141" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="9.52" height="1.219"/>
			</g>
			<g>
				<rect x="166.385" y="101.859" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="1.399" height="1.221"/>
				<rect x="169.024" y="101.859" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="5.569" height="1.221"/>
				<rect x="175.664" y="101.859" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="3.84" height="1.221"/>
			</g>
		</g>
	</g>
	<rect x="135.344" y="72.439" fill="#4FA8C9" width="23.801" height="25.07"/>
	<rect x="135.344" y="100.221" fill="#426757" width="23.801" height="15.159"/>
	<rect x="139.704" y="103.811" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="15.08" height="1.219"/>
	<g>
		<rect x="139.764" y="107.38" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.471" height="1.22"/>
		<rect x="148.274" y="107.38" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.45" height="1.22"/>
	</g>
	<g>
		<rect x="139.404" y="78.359" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.189" height="1.221"/>
		<rect x="147.145" y="78.359" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="7.939" height="1.221"/>
	</g>
	<g>
		<rect x="139.404" y="90.529" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.189" height="1.221"/>
		<rect x="147.145" y="90.529" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="7.939" height="1.221"/>
	</g>
	<g>
		<rect x="152.244" y="86.48" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="2.84" height="1.209"/>
		<rect x="139.404" y="86.48" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="11.38" height="1.209"/>
	</g>
	<g>
		<rect x="139.404" y="82.42" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="1.68" height="1.221"/>
		<rect x="142.564" y="82.42" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.66" height="1.221"/>
		<rect x="150.494" y="82.42" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="4.59" height="1.221"/>
	</g>
	<g>
		<rect x="139.754" y="110.96" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="1.601" height="1.21"/>
		<rect x="142.774" y="110.96" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.36" height="1.22"/>
		<rect x="150.354" y="110.96" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="4.38" height="1.22"/>
	</g>
</g>
<g class="celular">
	<path fill="#33485D" d="M60.22,125.26c0,1.86-1.51,3.381-3.38,3.381H34.12c-1.87,0-3.381-1.521-3.381-3.381v-47.8
		c0-1.87,1.511-3.38,3.381-3.38h22.72c1.87,0,3.38,1.51,3.38,3.38V125.26z"/>
	<path fill="#293A4C" d="M48.67,124.1c0,0.641-0.521,1.16-1.16,1.16h-4.43c-0.64,0-1.16-0.52-1.16-1.16c0-0.64,0.521-1.16,1.16-1.16
		h4.43C48.149,122.939,48.67,123.46,48.67,124.1z"/>
	<path fill="#5A6A7A" d="M46.44,78.1c0,0.541-0.431,0.971-0.961,0.971c-0.539,0-0.97-0.43-0.97-0.971c0-0.529,0.431-0.97,0.97-0.97
		C46.01,77.13,46.44,77.57,46.44,78.1z"/>
	<rect x="32.83" y="82.2" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="25.22" height="37.95"/>
	<rect x="34.181" y="84.35" fill="#FFD060" width="22.789" height="11.471"/>
	<rect x="36.75" y="87.641" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="17.649" height="1.209"/>
	<g>
		<rect x="34.181" y="114.5" fill="#F36B54" width="22.789" height="5.65"/>
	</g>
	<rect x="36.75" y="117.66" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="17.649" height="1.22"/>
	<g>
		<rect x="36.78" y="91.029" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="7.609" height="1.221"/>
		<rect x="46.229" y="91.029" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="8.141" height="1.221"/>
	</g>
	<rect x="34.181" y="97.76" fill="#4FA8C9" width="22.789" height="15.15"/>
	<g>
		<rect x="36.78" y="101.41" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="6.93" height="1.22"/>
		<rect x="45.45" y="101.41" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="8.88" height="1.22"/>
	</g>
	<g>
		<rect x="51.149" y="108.689" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="3.181" height="1.221"/>
		<rect x="36.78" y="108.689" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="12.74" height="1.221"/>
	</g>
	<g>
		<rect x="36.78" y="105.05" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="1.88" height="1.22"/>
		<rect x="40.319" y="105.05" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="7.451" height="1.22"/>
		<rect x="49.2" y="105.05" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="5.13" height="1.22"/>
	</g>
</g>
</svg>