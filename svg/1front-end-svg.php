<svg class="principles-icon principles-icon--frontend" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="200px" height="185.672px" viewBox="0 0 200 185.672" enable-background="new 0 0 200 185.672" xml:space="preserve">
<g id="js">
	<polygon fill="#EFDA4E" points="197,115.034 138.46,141.024 111.66,80.754 170.2,54.764 	"/>
	<g>
		<path d="M148.51,96.114l6.23-2.77l7.75,17.44c3.49,7.86,0.939,12.27-5.08,14.94c-1.471,0.659-3.46,1.25-4.87,1.38l-1.55-5.34
			c0.99-0.1,2.189-0.381,3.42-0.93c2.619-1.16,3.73-3.07,1.83-7.33L148.51,96.114z"/>
		<path d="M169.57,111.144c2.02,0.14,5.02-0.17,7.68-1.351c2.87-1.27,3.859-3.129,3.04-4.97c-0.75-1.68-2.511-2.12-6.34-1.8
			c-5.32,0.4-9.47-0.79-11.28-4.84c-2.09-4.71,0.3-10.03,6.77-12.9c3.15-1.4,5.681-1.79,7.66-1.74l0.83,5.61
			c-1.3-0.06-3.65,0.06-6.31,1.24c-2.71,1.2-3.45,3.05-2.83,4.44c0.78,1.76,2.64,1.86,6.86,1.66c5.68-0.37,9.06,1.17,10.8,5.1
			c2.06,4.63,0.28,10.119-7.29,13.479c-3.16,1.4-6.65,1.921-8.59,1.75L169.57,111.144z"/>
	</g>
</g>
<g id="code">
	<polygon fill="#9D9DA4" points="57.24,158.924 54.87,180.245 98,180.064 95.72,158.924 	"/>
	<path fill="#33485D" d="M148.59,140.444v-77.7c0-2.48-2.01-4.49-4.49-4.49H8.99c-2.48,0-4.49,2.01-4.49,4.49v77.7H148.59z"/>
	<path fill="#C8C7C4" d="M4.5,155.614c0,2.48,2.01,4.49,4.49,4.49H144.1c2.48,0,4.49-2.01,4.49-4.49v-15.17H4.5V155.614z"/>
	<path fill="#33485D" d="M79.029,150.144c0,1.399-1.129,2.54-2.539,2.54c-1.4,0-2.54-1.141-2.54-2.54s1.14-2.54,2.54-2.54
		C77.9,147.604,79.029,148.745,79.029,150.144z"/>
	<g>
		<defs>
			<rect id="SVGID_1_" x="10.17" y="64.284" width="132.38" height="70.471"/>
		</defs>
		<clipPath id="SVGID_2_">
			<use xlink:href="#SVGID_1_"  overflow="visible"/>
		</clipPath>

			<rect x="10.141" y="64.154" clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" fill="#373737" width="132.439" height="126.87"/>

			<rect x="18.54" y="78.794" clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" fill="#BFEFDF" width="11.51" height="2.09"/>

			<rect x="36.05" y="104.814" clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="24.92" height="2.09"/>
		<g clip-path="url(#SVGID_2_)">
			<rect x="32.21" y="100.404" fill-rule="evenodd" clip-rule="evenodd" fill="#BFEFDF" width="24.92" height="2.091"/>
			<rect x="61.01" y="100.404" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="24.91" height="2.091"/>
			<rect x="90.52" y="100.404" fill-rule="evenodd" clip-rule="evenodd" fill="#FDE07A" width="21.59" height="2.091"/>
		</g>

			<rect x="18.54" y="82.974" clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" fill="#FFE17B" width="22.31" height="2.09"/>

			<rect x="19.26" y="113.644" clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" fill="#FFE17B" width="8.631" height="2.09"/>

			<rect x="24.29" y="117.824" clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" fill="#BFEFDF" width="10.8" height="2.101"/>
		<g clip-path="url(#SVGID_2_)">
			<rect x="40.561" y="148.334" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="26.859" height="2.1"/>
			<rect x="72.561" y="148.334" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="26.859" height="2.1"/>
		</g>
		<g clip-path="url(#SVGID_2_)">
			<rect x="36.01" y="144.024" fill-rule="evenodd" clip-rule="evenodd" fill="#BFEFDF" width="7.21" height="2.09"/>
			<rect x="47.66" y="144.024" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="65.24" height="2.09"/>
		</g>
		<g clip-path="url(#SVGID_2_)">
			<rect x="31.83" y="139.704" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="19.76" height="2.09"/>
			<rect x="55.66" y="139.704" fill-rule="evenodd" clip-rule="evenodd" fill="#FFE17B" width="9.58" height="2.09"/>
			<rect x="69.85" y="139.704" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="17.76" height="2.09"/>
			<rect x="92.05" y="139.704" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="33.04" height="2.09"/>
		</g>

			<rect x="27.47" y="135.394" clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="40.31" height="2.09"/>
		<g clip-path="url(#SVGID_2_)">
			<rect x="23.1" y="131.074" fill-rule="evenodd" clip-rule="evenodd" fill="#BFEFDF" width="15.58" height="2.091"/>
			<rect x="43.13" y="131.074" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="40.32" height="2.091"/>
			<rect x="87.32" y="131.074" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="40.32" height="2.091"/>
		</g>
		<g clip-path="url(#SVGID_2_)">
			<rect x="27.891" y="122.014" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="15.84" height="2.09"/>
			<rect x="94.12" y="122.014" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="12.95" height="2.09"/>
			<rect x="48.05" y="122.014" fill-rule="evenodd" clip-rule="evenodd" fill="#FFE17B" width="42.47" height="2.09"/>
		</g>
		<g clip-path="url(#SVGID_2_)">
			<rect x="27.891" y="96.214" fill-rule="evenodd" clip-rule="evenodd" fill="#FFE17B" width="11.52" height="2.1"/>
			<rect x="104.189" y="96.214" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="20.881" height="2.1"/>
			<rect x="43.73" y="96.214" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" width="56.149" height="2.1"/>
		</g>

			<rect x="35.09" y="78.794" clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="18.721" height="2.09"/>

			<rect x="23.57" y="92.034" clip-path="url(#SVGID_2_)" fill-rule="evenodd" clip-rule="evenodd" fill="#666970" width="39.59" height="2.09"/>
	</g>
	<rect x="49.189" y="179.495" fill="#C8C7C4" width="54.971" height="3.979"/>
</g>
<g id="css3">
	<polygon fill="#1E61AD" points="127.109,57.954 109.359,76.614 84.57,83.654 47,32.654 99.35,1.024 	"/>
	<polygon fill="#337CC5" points="75.439,20.574 106.811,72.414 120.97,56.864 97.29,7.374 	"/>
	<polygon fill="#FFFFFF" points="95.6,18.604 64.27,37.534 69.52,44.564 84.48,35.524 73.37,49.864 78.6,56.934 98.939,44.644
		102.82,52.504 98.04,57.914 91.029,59.584 88.18,56.254 81.2,60.464 87.7,69.264 102.561,65.394 112.88,54.044 102.29,32.384
		87.91,41.074 99.439,26.484 	"/>
</g>
<g id="html5">
	<g>
		<polygon fill="#E34C25" points="106.391,22.974 100.87,86.204 124.57,97.543 150.79,95.054 167.42,33.814 		"/>
	</g>
	<g>
		<path fill-rule="evenodd" clip-rule="evenodd" fill="#EAEAEA" d="M120.23,72.594l0.539-6.19l-7.67-1.36l-1.06,12.18l14.88,7.12
			l0.04-0.01l1.41-7.94h-0.04L120.23,72.594z M134.641,41.114l-19.16-3.4l-0.181,2.08l-1.86,21.37l17.101,3.04l1.351-7.63
			l-10.111-1.8l0.69-7.94l10.81,1.92L134.641,41.114z"/>
	</g>
	<g>
		<polygon fill="#F06428" points="136,33.484 125.55,92.264 146.74,90.254 160.95,37.914 		"/>
	</g>
	<g>
		<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M134.609,41.114l-1.35,7.63l18.45,3.28l0.46-1.69l1.04-3.81
			l0.54-2.01L134.609,41.114z M130.51,64.194l9.41,1.67l-2.65,9.75l-8.93,0.78l-1.41,7.94l16.43-1.55l0.351-1.27l5.37-19.78
			l0.55-2.02l-17.76-3.15L130.51,64.194z"/>
	</g>
</g>
</svg>
