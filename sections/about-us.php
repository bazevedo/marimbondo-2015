<section class="section section--about-us">
	<div class="content about-us">
		<div class="row center-elements">
			<h2 id="about-us" class="about-us-title default-text default-text--bold default-text--size-9 default-text--color-2">Olá! Somos a marimbondo.</h2>
			<p class="about-us-subheader default-text default-text--paragraph default-text--size-4 default-text--color-3">E estes são os nossos 5 princípios de desenvolvimento:</p>
		</div>
		<div class="principles-wrapper">
			<ul class="row principles-list" id="js-principles-list">
				<li class="principles-list__item principles-list__item--accessibility">
					<a class="principles-list__anchor" href="#accessibility" data-principle="accessibility">
						<?php get_template_part('svg/hexagono-svg'); ?>
						<?php get_template_part('svg/acessibilidade-svg'); ?>
						<strong class="principles-list__title default-text default-text--bold default-text--size-3 default-text--color-4 default-text--uppercase">acessibilidade</strong>
					</a>
				</li>
				<li class="principles-list__item principles-list__item--ux">
					<a class="principles-list__anchor" href="#ux" data-principle="ux">
						<?php get_template_part('svg/hexagono-svg'); ?>
						<?php get_template_part('svg/ux-ui-svg'); ?>
						<strong class="principles-list__title default-text default-text--bold default-text--size-3 default-text--color-5 default-text--uppercase">UX/UI Design</strong>
					</a>
				</li>
				<li class="principles-list__item principles-list__item--responsive">
					<a class="principles-list__anchor" href="#responsive" data-principle="responsive">
						<?php get_template_part('svg/hexagono-svg'); ?>
						<?php get_template_part('svg/responsive-svg'); ?>
						<strong class="principles-list__title default-text default-text--bold default-text--size-3 default-text--color-5 default-text--uppercase">Responsivo</strong>
					</a>
				</li>
				<li class="principles-list__item principles-list__item--frontend">
					<a class="principles-list__anchor" href="#frontend" data-principle="frontend">
						<?php get_template_part('svg/hexagono-svg'); ?>
						<?php get_template_part('svg/front-end-svg'); ?>
						<strong class="principles-list__title default-text default-text--bold default-text--size-3 default-text--color-5 default-text--uppercase">Front-End</strong>
					</a>
				</li>
				<li class="principles-list__item principles-list__item--wordpress">
					<a class="principles-list__anchor" href="#wordpress" data-principle="wordpress">
						<?php get_template_part('svg/hexagono-svg'); ?>
						<?php get_template_part('svg/wordpress-svg'); ?>
						<strong class="principles-list__title default-text default-text--bold default-text--size-3 default-text--color-5 default-text--uppercase">WordPress</strong>
					</a>
				</li>
			</ul>
			<div class="sequence visuallyhidden" id="js-sequence" role="main">
				<ul class="principles-overlay sequence-canvas" id="js-principles-overlay">
					<li class="principles-overlay__item principles-overlay__item--accessibility animate-in" data-principle="accessibility">
						<div class="principles-overlay-info">
							<h3 id="accessibility" class="principles-overlay__title default-text default-text--bold default-text--size-6 default-text--color-4">Foco na Acessibilidade</h3>
							<p class="default-text default-text--paragraph default-text--size-3 default-text--color-4">A web deve ser para todos. Por isso, não medimos esforços para tornar os nossos sites acessíveis ao maior número possível de pessoas, sempre desenvolvendo soluções para aqueles que demandam uma navegação adaptada.</p>
						</div>
					</li>
					<li class="principles-overlay__item principles-overlay__item--ux animate-out" data-principle="ux">
						<div class="principles-overlay-info">
							<h3 id="ux" class="principles-overlay__title default-text default-text--bold default-text--color-1 default-text--size-6">UX/UI Design</h3>
							<p class="default-text default-text--paragraph default-text--size-3 default-text--color-1">Mais que bonitinho, um site precisa ser funcional. A experiência do usuário e a organização das informações são fundamentais, portanto a navegação pelo site deve ser agradável e intuitiva.</p>
						</div>
					</li>
					<li class="principles-overlay__item principles-overlay__item--responsive animate-out" data-principle="responsive">
						<div class="principles-overlay-info">
							<h3 id="responsive" class="principles-overlay__title default-text default-text--bold default-text--size-6 default-text--color-1">Design Responsivo</h3>
							<p class="default-text default-text--paragraph default-text--size-3 default-text--color-1">Nossos sites se adaptam a qualquer tamanho de tela e são navegáveis em todos os dispositivos, do computador ao smartphone, do tablet à televisão. Assim, garantimos a melhor experiência para o seu usuário, de onde quer que ele acesse.</p>
						</div>
					</li>
					<li class="principles-overlay__item principles-overlay__item--frontend animate-out" data-principle="frontend">
						<div class="principles-overlay-info">
							<h3 id="frontend" class="principles-overlay__title default-text default-text--bold default-text--size-6 default-text--color-1">Front-End Performático</h3>
							<p class="default-text default-text--paragraph default-text--size-3 default-text--color-1">Sempre com a performance como um requisito básico do projeto, desenvolvemos nossos sites de maneira que sejam leves e rápidos, utilizando HTML5, CSS3 e JavaScript, além de seguir os padrões de desenvolvimento do W3C.</p>
						</div>
					</li>
					<li class="principles-overlay__item principles-overlay__item--wordpress animate-out" data-principle="wordpress">
						<div class="principles-overlay-info">
							<h3 id="wordpress" class="principles-overlay__title default-text default-text--bold default-text--size-6 default-text--color-1">Soluções em WordPress</h3>
							<p class="default-text default-text--paragraph default-text--size-3 default-text--color-1">Nossa principal ferramenta para administração dos sites de nossos clientes. Gratuita, segura e altamente customizável, ela é uma plataforma confiável para o desenvolvimento de sites de todos os tamanhos.</p>
						</div>
					</li>
				</ul>
				<ul class="principles-commmands" id="js-principles-commmands">
					<li class="principles-commmands__item"><button tabindex="-1" class="default-button principles-commmands__navigation principles-commmands__navigation--prev" id="js-principles-commmands__prev" type="button" title="Ir para o princípio anterior">anterior</button></li>
					<li class="principles-commmands__item"><button tabindex="-1" class="default-button principles-commmands__navigation principles-commmands__navigation--next" id="js-principles-commmands__next" type="button" title="Ir para o próximo princípio">próximo</button></li>
					<li class="principles-commmands__item"><button tabindex="-1" class="default-button principles-commmands__close" id="js-principles-commmands__close" type="button" title="Fechar os princípios">fechar</button></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<hr class="content horizontal-breaker horizontal-breaker--about-us" />