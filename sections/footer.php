<!-- FOOTER -->
<div class="section section--footer">
	<div class="content footer-container">
		<strong class="default-text default-text--size-10 default-text--bold default-text--uppercase">Obrigado<span class="footer-container__thanks default-text--size-7">pela visita!</span></strong>
		<span class="footer-container__mascot">&nbsp;</span>
	</div>
</div>
<!-- END FOOTER -->