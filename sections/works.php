<!-- WORKS -->
<section class="section section--works">
	<div class="content works-container">
		<div class="center-elements">
			<h2 id="works" class="works-container__title default-text default-text--size-6 default-text--color-6 default-text--bold">Aqui estão alguns de nossos clientes incríveis:</h2>
		</div>
		<ul class="works-list">
			<li class="works-list__item col-3-12">
				<img class="works-list__logo" alt="W3C Brasil" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/works/w3c.svg" />
			</li>
			<li class="works-list__item col-3-12">
				<img class="works-list__logo" alt="Nic.BR" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/works/nic-br.svg" />
			</li>
			<li class="works-list__item col-3-12">
				<img class="works-list__logo" alt="BrazilJS Foundation" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/works/braziljs.svg" />
			</li>
			<li class="works-list__item col-3-12">
				<img class="works-list__logo" alt="KingHost" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/works/kinghost.svg" />
			</li>
			<li class="works-list__item col-3-12">
				<img class="works-list__logo" alt="Secretaria dos direitos humanos da presidência da republica" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/works/secretaria-dos-direitos-humanos.png" />
			</li>
			<li class="works-list__item col-3-12">
				<img class="works-list__logo" alt="Instituto Vladimir Herzog" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/works/instituto-vladimir-herzog.png" />
			</li>
			<li class="works-list__item col-3-12">
				<img class="works-list__logo" alt="Programa das Nações Unidas para o Desenvolvimento Brasil" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/works/pnud.svg" />
			</li>
			<li class="works-list__item col-3-12">
				<img class="works-list__logo" alt="Liquid Media Lab" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/works/liquid-media-lab.svg" />
			</li>
		</ul>
		<div class="center-elements">
			<a class="work-container__see-works default-text default-text--size-4 default-text--color-1 default-text--bold default-text--uppercase" href="http://behance.net/marimbondo">Veja nossos trabalhos no Behance</a>
		</div>
	</div>
</section>
<!-- END WORKS -->