<header class="section section--home-header" role="banner">
	<div class="content home-header">
		<div class="row">
			<div class="col-6-12">
				<h1 itemprop="name" class="home-header__logo">Marimbondo - web com gominho</h1>
				<span class="default-icon default-icon__cloud default-icon__cloud--header">&nbsp;</span>
				<span class="default-icon default-icon__cloud default-icon__cloud--reversed">&nbsp;</span>
				<span class="default-icon default-icon__cloud default-icon__cloud--large">&nbsp;</span>
				<span class="default-icon default-icon__cloud default-icon__cloud--reversed-small">&nbsp;</span>
			</div>
			<div class="col-6-12 last">
				<div class="navigation-wrapper" id="js-navigation-wrapper">
					<nav class="home-navigation" role="navigation">
						<button aria-hidden="true" id="js-navigation-toogler" class="default-button navigation-toogler" type="button"><span class="navigation-toogler__burguer"></span><span class="navigation-toogler__text default-text default-text--bold default-text--color-1 default-text--size-3">menu</span></button>
						<ul class="home-navigation__list" id="js-navigation-list">
							<li class="home-navigation__item"><a class="home-navigation__anchor default-text default-text--size-5 default-text--bold default-text--color-1" href="#about-us">o que fazemos</a></li>
							<li class="home-navigation__item"><a class="home-navigation__anchor default-text default-text--size-5 default-text--bold default-text--color-1" href="#works">nossos trabalhos</a></li>
							<li class="home-navigation__item"><a class="home-navigation__anchor default-text default-text--size-5 default-text--bold default-text--color-1" href="#contact-name">contato</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<div class="col-6-12 home-header__about">
			<p class="home-header__sub-header default-text default-text--color-1 default-text--size-8 default-text--bold">Sites desenvolvidos artesanalmente, com o respeito que todo usuário merece</p>
			<p class="home-header__about default-text default-text--color-1 default-text--size-4">Acreditamos em uma web para todos. Especialistas em UX e front-end, nós criamos sites responsivos e acessíveis, que se adaptam a qualquer dispositivo e são legíveis por todos os públicos.</p>
			<div class="home-header__lets-work-container">
				<a id="js-home-header__lets-work" href="#contact-name" class="home-header__lets-work anchor-button"><span class="anchor-button__text-container"><span class="anchor-button__text-side-1">Vamos trabalhar juntos?</span><span class="anchor-button__text-side-2">Entre em contato!</span></span></a>
			</div>
		</div>
		<div class="col-6-12 last home-header__mascot-holder">
			<img class="home-header__mascot" alt="Marimbondo voando" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/logos/marimbonda.svg" />
		</div>
	</div>
	<span id="js-home-header__uptown" class="home-header__uptown">&nbsp;</span>
	<span id="js-home-header__middletown" class="home-header__middletown">&nbsp;</span>
	<span id="js-home-header__lowertown" class="home-header__lowertown">&nbsp;<span class="home-header__lowertown--diagonal-space">&nbsp;</span></span>
</header>