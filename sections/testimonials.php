<!-- TESTIMONIALS -->
<div class="section section--testimonials">
	<div class="content row">
		<section class="testimonials-container col-8-12">
			<h2 class="testimonials-container__title default-text default-text--size-6 default-text--color-6 default-text--bold">Opinião de quem trabalhou com a gente</h2>
			<div class="col-4-12">
				<div class="col-3-12">
					<img class="testimonials-container__thumb" alt="Yaso Cordova" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/testimonials/yaso-cordova.jpg" />
					<p class="testimonials-container__testimonial default-text default-text--paragraph default-text--color-7 default-text--size-1">O trabalho da Marimbondo é muito fera. São profissionais, respeitam prazos, tranquilos e tratam o site que a gente demanda como se fosse roupa sob medida: cada detalhe é pensado junto, não tem nada que seja feito às pressas ou com gambiarras.</p>
					<p class="testimonials-container__testimonial default-text default-text--paragraph default-text--color-7 default-text--size-1">Outra coisa legal é que o time da Marimbondo se adapta às suas ferramentas: se você usa github, eles vão de github. Se você usa bitbucket, vão de bitbucket, sem problemas. Ou seja: é uma empresa completamente voltada pra experiência do cliente, preocupada em atingir o público-alvo desejado ao mesmo tempo em que desenvolve projetos Web elegantes e criativos.</p>
					<p class="testimonials-container__testimonial default-text default-text--paragraph default-text--color-7 default-text--size-1">Ah e o preço da Marimbondo é muito honesto. A Marimbondo tem a qualidade de uma botique de desenvolvimento Web, com a simplicidade e aproximação que se tem quando se trabalha com freelancers.</p>
					<strong class="testimonials-container__name default-text default-text--bold default-text--size-4 default-text--color-8">Yaso Córdova</strong>
					<em class="testimonials-container__profession default-text default-text--paragraph default-text--size-1 default-text--color-8">W3C Brasil</em>
				</div>
			</div>
			<div class="col-4-12 last">
				<div class="col-3-12">
					<img class="testimonials-container__thumb" alt="Jaydson Gomes" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/testimonials/jaydson-gomes.jpg" />
					<p class="testimonials-container__testimonial default-text default-text--paragraph default-text--color-7 default-text--size-1">Conheço a Marimbondo desde antes mesmo do seu nascimento, e talvez esse seja um dos motivos de minha admiração e respeito.</p>
					<p class="testimonials-container__testimonial default-text default-text--paragraph default-text--color-7 default-text--size-1">A ideia de desenvolver "sites artesanais" é algo inovador em um mercado cada vez mais burocrático(chato) e cheio de cópias.</p>
					<p class="testimonials-container__testimonial default-text default-text--paragraph default-text--color-7 default-text--size-1">Sou apaixonado pelo Web e levo o desenvolvimento de websites muito a sério, e é por isso que acho que uma empresa como a Marimbondo merece créditos pelo seu excelente trabalho.</p>
					<strong class="testimonials-container__name default-text default-text--bold default-text--size-4 default-text--color-8">Jaydson Gomes</strong>
					<em class="testimonials-container__profession default-text default-text--paragraph default-text--size-1 default-text--color-8">BrazilJS</em>
				</div>
			</div>
		</section>
		<div class="col-4-12 last">
			<div class="social-networks-container">
				<div id="js-facebook-frame">&nbsp;</div>
				<div class="social-networks-container__find-us">
					<h2 class="social-networks-container__title">Encontre a marimbondo nas redes sociais:</h2>
					<ul class="social-networks-list">
						<li class="social-networks-list__item">
							<a href="https://www.facebook.com/marimbondo.me" class="social-networks-list__anchor social-networks-list__anchor--facebook" title="Acesse nosso Facebook"><span class="social-networks-list__icon">Facebook</span></a>
						</li>
						<li class="social-networks-list__item">
							<a href="https://www.linkedin.com/company/3097137?trk=prof-exp-company-name" class="social-networks-list__anchor social-networks-list__anchor--linkedin" title="Acesse nosso Linkedin"><span class="social-networks-list__icon">Linkedin</span></a>
						</li>
						<li class="social-networks-list__item">
							<a href="http://be.net/marimbondo" class="social-networks-list__anchor social-networks-list__anchor--behance" title="Acesse nosso Behance"><span class="social-networks-list__icon">Behance</span></a>
						</li>
					</ul>
				</div>

				<hr class="content horizontal-breaker horizontal-breaker--content" />
				<div class="location-container">
					<address class="default-text default-text--paragraph default-text--size-2">
						<img class="location-container__logo" alt="Logo marimbondo com asas" src="<?php echo get_template_directory_uri(); ?>/<?php echo getEnvironment(); ?>/img/logos/logo-marimbondo-asas.svg" />
						<a class="location-container__mail default-text--color-8" href="mailto:contato@marimbondo.me?subject=Oi!" title="Envie um e-mail pra gente"><span class="default-icon default-icon--email">&nbsp;</span><span class="location-container__mail-text" itemprop="email">contato@marimbondo.me</span></a>
						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="location-container__address default-text--color-9"><span itemprop="streetAddress">Rua Garibaldi, 1114/26</span> <br/> <span itemprop="addressLocality">Bom Fim</span>, <span itemprop="addressRegion">Porto Alegre <br/> Rio Grande do Sul, Brasil</span></p>
						<p class="location-container__phone default-text--bold default-text--color-9" itemprop="telephone"><span class="default-icon default-icon--phone">&nbsp;</span>+55 51 3273.3749</p>
					</address>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END TESTIMONIALS -->