<!-- CONTACT -->
<section class="section section--contact">
	<div class="content contact-container">
		<form method="POST" action="<?php echo get_template_directory_uri(); ?>/scripts/sendMail.php" id="js-contact-form" class="contact-form default-text default-text--color-1 default-text--size-7 center-elements">
			<h2 id="contact-form" class="visuallyhidden">Entre em contato com a gente:</h2>
			<label for="contact-name" class="">Olá, meu nome é</label>
			<input aria-label="Por favor, digite seu nome" id="contact-name" class="contact-form__input default-text--size-6" name="contact-name" type="text" required />
			<label for="contact-mail">e quero fazer um projeto com vocês! Por favor, envie um e-mail para</label>
			<input aria-label="Por favor, digite seu e-mail" id="contact-mail" class="contact-form__input contact-form__input--mail default-text--size-6" name="contact-mail" type="email" required />.
			<span class="contact-form__thanks">Obrigado.</span>
			<div class="anchor-button__wrapper">
				<button class="contact-form__submit anchor-button" type="submit"><span class="anchor-button__text-container"><span class="anchor-button__text-side-1 anchor-button__text-side-1--contact-form">Enviar contato</span><span class="anchor-button__text-side-2 anchor-button__text-side-2--contact-form">Vamos trabalhar juntos!</span></span></button>
			</div>
		</form>
	</div>
	<div class="center-elements">
		<p class="default-text default-text--paragraph default-text--size-1 default-text--color-1">Se preferir, envie um e-mail direto para <a itemprop="email" class="default-text--color-10" href="mailto:contato@marimbondo.me?subject=Oi!" title="Envie um e-mail pra gente :)">contato@marimbondo.me</a></p>
	</div>
</section>
<!-- END CONTACT -->